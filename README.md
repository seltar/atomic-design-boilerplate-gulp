Atomic Design Boilerplate Gulp edition
=====================
> Yonas Sandbaek [@seltar](https://twitter.com/seltar), [github](https://github.com/seltar), [bitbucket](https://bitbucket.org/seltar)  
> https://bitbucket.org/seltar/atomic-design-boilerplate-gulp

###### Version
> 0.3.0

### What is this
This project aims to give structure and improve automation when working with atomic design principles.

### What does it do?
- Separates between development and production.
- Compiles handlebars templates with partials, helpers and external data to html
- Compiles, autoprefixes and minifies sass files to css
- Lints, compiles and minifies js with requirejs optimizer

### Known issues
- gulp.watch doesn't react to newly added files. Rerun gulp as a fix for now. 

### Starting up
- Clone the [repo](https://seltar@bitbucket.org/seltar/atomic-design-boilerplate-gulp.git)
- Open up terminal and install dependencies with `[sudo] npm install`
- Run `gulp` to build and start watching
- Go forth and write your jargon, you little codemonkey!

### Configuration
- To change the folder configuration or site information, edit `config.js`


### Building
Here are the tasks you can use with gulp

- Build html, compile javascript, compile sass, start server and watch for changes

		gulp

- Add a `--production` flag to run production tasks

		i.e. gulp --production

- Build all

		gulp build

- Clean folders

		gulp clean

- Start server and watch for changes
	
		gulp server

- Build html

		gulp html

- Lint and compile javascript

		gulp scripts 

- Generate SASS import for components and compile SASS

		gulp styles 

- Generate documentation with [docker](http://jbt.github.io/docker/src/docker.js.html)

		npm run docs


### More info / documentation

[Handlebars template language](http://handlebarsjs.com/)
[Layout helper for Handlebars](https://github.com/shannonmoeller/handlebars-layouts)



---

### Folder structure
	root
	│
	├───.gitignore
	├───config.js
	├───gulpfile.js
	├───generate-imports.js
	├───package.json
	├───README.md
	│
	├───app
	│   │
	│   ├───assets
	│   │   │   
	│   │   ├───ajax
	│   │   │   
	│   │   ├───dummy-images
	│   │   │   
	│   │   ├───fonts
	│   │   │   
	│   │   └───images
	│   │       
	│	│───components
	│	│   │
	│	│   ├───_helpers
	│	│   │   │   
	│	│   │   │   # all .js files in this folder are included as handlebars helpers.
	│	│   │   └───atomic.js
	│	│   │
	│	│   ├───_partials
	│	│   │   │
	│	│   │   │   # layouts
	│	│   │   ├───layout.hbs
	│	│   │   ├───layout-alt.hbs
	│	│   │   │
	│	│   │   │   # all .hbs files in this folder are included as handlebars partials.
	│	│   │   ├───head.hbs
	│	│   │   └───javascripts.hbs
	│	│   │       
	│	│   │       
	│	│   ├───inline-templates
	│	│   │   │
	│	│   │   │   # Templates used in the live app
	│	│   │       
	│	│   ├───atoms
	│	│   │   │
	│	│   │   │   # Atoms
	│	│   │   └───example
	│	│   │       │   # styles
	│	│   │       ├───_example.scss
	│	│   │       │   # template
	│	│   │       ├───example.hbs
	│	│   │       │   # script
	│	│   │       ├───example.js
	│	│   │       │   # unit test
	│	│   │       └───example.test.js
	│	│   │       
	│	│   ├───molecules
	│	│   │   │
	│	│   │   │   # Molecules
	│	│   │       
	│	│   ├───organisms
	│	│   │   │
	│	│   │   │   # Organisms
	│	│   │       
	│	│   └───templates
	│	│       │
	│	│       │   # Templates
	│   │       
	│   ├───data
	│   │       
	│   ├───js
	│   │   │   
	│   │   └───vendor
	│   │       
	│   └───sass
	│           
	│           
	└─node_modules

--- 

### Changelog



##### 12.01.2016

- All variables with "name" needs to be changed as it now refers to the name of the component i.e. 'atom-example'

- Template format and setting of title, body-class etc. has changed

	{{#extend 'layout' title="Home" body-class="index" dummyVar="dummyVarExample" }}

		{{#content 'body'}}
			Fill in your content here. It will be placed in the {{#blocks 'body'}} block in 'layout.hbs'
		{{/content}}

		{{#content 'scripts'}}
			Add scripts that need to be run on this template 
		{{/content}}

	{{/extend}}

	Look in layout.hbs in components/_partials to see which {{#blocks}} are available
	and check out [handlebars-layout](https://github.com/shannonmoeller/handlebars-layouts) for more documentation

- _layouts folder is removed and content is moved into _partials. All layouts now have the prefix 'layout'

