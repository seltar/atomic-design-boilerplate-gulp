/**
 * Gulpfile for Atomic Design Boilerplate
 *
 * Copyright (c) 2015 Yonas Sandbæk
 * Licensed under the MIT License (MIT).
 * 
 */

var argv = require('yargs').argv,
	production = !!(argv.production), // true if --production flag is used

// config
	pkg = require('./package.json'),
	config = require('./config.js'),
	paths = config.paths,

// gulp
	gulp = require('gulp'),
	gulpif = require('gulp-if'),
	extend = require('node.extend'),
	rename = require('gulp-rename'),
	// debug = require("gulp-debug"),
	plumber = require("gulp-plumber"),

// watch
	watch = require('gulp-chokidar')(gulp),

// server
	browserSync = require('browser-sync').create(),
	reload      = browserSync.reload,
	filter      = require('gulp-filter'),

// js
	jshint = require('gulp-jshint'),
	uglify = require('gulp-uglify'),
	rjs = require('gulp-requirejs'),

// css
	sass = require('gulp-sass'),
	sassGlob = require('gulp-sass-glob'),
	autoprefixer = require('gulp-autoprefixer'),
	sourcemaps = require('gulp-sourcemaps'),
	uglifycss = require('gulp-uglifycss'),
 
// html
	hb = require('gulp-hb'),
	data = require('gulp-data'),

	through = require('through2'),
	fs = require('fs'),
	path = require("path"),

// cleanup
	del = require('del');

/**
 * Tasks
 */


var dataObj = {};

gulp.task('html:data', function() {
	dataObj = {
		production: production,
		site: config.site
	};

	return gulp
		.src(paths.layout.data + "**/*.{js,json}")
		.pipe(data(function(file, cb){
			dataObj[path.basename(file.path).replace(".json", "")] = JSON.parse(String(file.contents));
			cb();
		}));
});

// Build html Task
gulp.task('html', ['html:data'], function() {

	return gulp
		.src(paths.layout.src + "**/*.hbs")
		.pipe(plumber())
		.pipe(hb({
			bustCache: true,
			data: dataObj,
			helpers: paths.layout.helpers + "**/*.js",
			partials: paths.layout.partials + "**/*.hbs"
		}))
		.pipe(rename({extname: ".html"}))
		.pipe(gulpif(!production, gulp.dest(paths.layout.dest)))
		.pipe(gulpif(production, gulp.dest(paths.layout.build)));
});

// Copy assets Task
gulp.task('assets', function() {
	return gulp
		.src(paths.assets.src + "**/*")
		.pipe(gulpif(!production, gulp.dest(paths.assets.dest)))
		.pipe(gulpif(production, gulp.dest(paths.assets.build)));
});

// Lint scripts Task
gulp.task('lint', function() {
	return gulp
		.src([
			paths.scripts.src + "**/*.js",
			paths.layout.components + "**/*.js",
			// exclude vendor scripts
			"!" + paths.scripts.src + "vendor/**/*.js",
			"!" + paths.layout.components + "_helpers/atomic.js"
		])
		.pipe(jshint())
		.pipe(jshint.reporter('default'));
});

// Vendor scripts Task
gulp.task('scripts:external', function(cb){
	if(!production){
		cb();
		return;
	}
	// add external scripts
	return gulp.src([
			paths.scripts.src + "vendor/**/*.js",
			paths.scripts.src + "template/**/*.js"
		], { base: paths.scripts.src })
		.pipe(gulp.dest(paths.scripts.build));
});

// Compile scripts Task
gulp.task('scripts:build', ['scripts:external'], function(){
	if(!production){
		return;
	}
	// compile app
	rjs({
		baseUrl: paths.scripts.src,
		mainConfigFile: paths.scripts.src + "config.js",
		name: "app",
		out: 'app.js'
	})
	.pipe(gulp.dest(paths.scripts.build))
	.pipe(uglify())
	.pipe(rename({extname: ".min.js"}))
	.pipe(gulp.dest(paths.scripts.build));
});

// Components scripts task
gulp.task('scripts:components', function(cb) {
	if(production){
		cb();
		return;
	}
	return gulp
		.src([
			paths.layout.components + "**/*.js",
			"!" + paths.layout.components + "_helpers/**/*.js"
		])
		.pipe(gulp.dest(config.path.dest + config.layout.components));
});

// Build scripts task
gulp.task('scripts', ['lint', 'scripts:components', 'scripts:build'], function(cb) {
	if(production){
		cb();
		return;
	}

	return gulp
		.src(paths.scripts.src + "**/*.js")
		.pipe(gulp.dest(paths.scripts.dest));
});

// Compile styles Task
gulp.task('styles', function (){
	return gulp
		.src(paths.styles.src+ "**/*.scss")

		.pipe(sourcemaps.init())
		.pipe(sassGlob())
		.pipe(sass({errLogToConsole: true}))
		.on('error', function onError(err) {
			console.log(err);
			this.emit('end');
		})
		.pipe(sourcemaps.write({sourceRoot: '.'}))
		
		.pipe(sourcemaps.init({ loadMaps: true }))
		.pipe(autoprefixer())
		.pipe(sourcemaps.write(".", {sourceRoot: '.'}))


		.pipe(gulpif(!production, gulp.dest(paths.styles.dest)))
		.pipe(gulpif(production, gulp.dest(paths.styles.build)))

		.pipe(filter('**/*.css'))
        .pipe(reload({stream:true}))

        // minify
		.pipe(filter(['**/*.css', '!**/*.min.css']))
		.pipe(uglifycss({
			"max-line-len": 80
		}))
		.pipe(rename({extname: '.min.css'}))
		.pipe(gulpif(!production, gulp.dest(paths.styles.dest)))
		.pipe(gulpif(production, gulp.dest(paths.styles.build)));
});

// Clean html Task
gulp.task('clean', function(cb) {
	if(production){
		del([config.path.build], cb);
	}else{
		del([config.path.dest], cb);
	}
});

// Static server
gulp.task('browser-sync', ['build'], function() {
	var basepath = production ? paths.layout.build : paths.layout.dest;
    browserSync.init({
        server: {
            baseDir: basepath
        }
    });

	watch([
		paths.scripts.src + "**/*.js",
		paths.layout.components + "**/*.js",
		"!" + paths.layout.components + "_helpers/atomic.js"
	], ['scripts']);

	watch([
		paths.styles.src + "**/*.scss",
		paths.layout.components + "**/*.scss"
	], ['styles']);

	watch(paths.assets.src + "**/*.*", ['assets']);

	watch([
		paths.layout.src + "**/*.hbs",
		paths.layout.data + "**/*.{js,json}",
		paths.layout.helpers + "**/*.js",
		paths.layout.components + "**/*.hbs"
	], ['html']);

	// reload browser event
    gulp.watch(basepath + "**/*.js").on('change', reload);
    gulp.watch(basepath + "**/*.html").on('change', reload);
});

gulp.task('build', ['html', 'styles', 'scripts', 'assets']);
gulp.task('server', ['browser-sync']);
gulp.task('default', ['server']);
